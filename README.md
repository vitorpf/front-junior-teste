# Teste Front-end Jr

# Objetivo 
O teste visa alinhar se o conhecimento do candidato está dentro da expectativa para a posição 

# Por onde começar? 

* Dê um fork do projeto para a sua conta do BitBucket
* Adicione uma branch test-dev
* Coloque todos os arquivos do seu projeto na pasta "src"
* Quando finalizar o teste envie o link do seu repositório para thiago.paulo@zebradeluxe.com com o assunto: Teste Front-end Jr

# Frontend Test 

O teste compreende 2 etapas:  

1 - Criar uma landing page a partir de um PSD  

2 - Fazer com que essa landing page seja um tema do WordPress com seções editáveis pelo painel de admin  




# 1 etapa - Criação da landing page
Criar uma landing page responsiva a partir do layout contido no arquivo "index.psd". Sinta-se livre para usar frameworks CSS (preferência por Bootstrap 4), plugins/bibliotecas de jQuery, etc

Não é obrigatório adaptar o layout de forma 100% fiel ao PSD, mas estaremos observando os quesitos abaixo:

Objetivos essenciais esperados  

- Responsivo com menu "hamburger"  

- Menu fixo, acompanha a página na rolagem  

- Menu com click e rolagem para a seção apropriada (sinta-se livre para renomear os menus como quiser)  

- Carousel / Swiper com troca automática na seção de destaque no topo (Welcome to Bino)  

- Na seção Recent Works fazer o efeito de hover (como indicado no segundo quadrado) com o overlay vermelho transparente + texto  




Objetivos não-obrigatórios (mas legal se souber/conseguir fazer)  

- Implementação totalmente fiel, incluindo forma triangular abaixo da seção principal do topo e demais detalhes  

- Efeitos de animação/delay nas transições, hovers, etc  

- Deixar o menu em vermelho (selecionado/active) quando estiver por cima da seção equivalente  

- Qualquer outra ideia que tiver para enriquecer a experiência do usuário e/ou demonstrar seus conhecimentos  


# 2 etapa - Adaptação para tema do WordPress
Utilizamos o WordPress na agência, portanto é importante avaliar a familiaridade do candiadato com esse CMS  


Sinta-se livre para usar temas em branco como ponto de partida e plugins, caso julgue necessário. As áreas editáveis pelo painel deverão ser as seguintes:  

- Topo (carousel)  

- Our Blog  



Objetivos essenciais esperados  

- Criar uma categoria "banner" e os títulos + resumos dos posts dessa categoria devem aparecer no carousel do topo  

- Seção our blog mostrar os quatro últimos posts (no quadrado vermelho: título, resumo e link para o post) que não estejam na categoria "banner"  

- Criar o template "single.php" de post aberto pra onde os links de cada post levam. Usar header e footer do layout  




Objetivos não obrigatórios (mas legal se souber/conseguir fazer)  

- Fazer cada item do banner/carousel ter sua própria imagem (podendo ser upada/trocada pelo painel do WordPress)  

- Fazer cada item da seção Our blog ter sua própria imagem (podendo ser upada/trocada pelo painel do WordPress)  

- Qualquer outra ideia que tiver para demonstrar seus conhecimentos em WordPress  



# Documentação 
Acreditamos que não existe código certo ou errado, afinal desenvolver é uma arte e existem diversas maneiras de se chegar em uma solução, então para podermos avaliar seu código da melhor forma adicione um arquivo de documentação no projeto explicando a sua solução  


# Passos para rodar os projetos 
Também adicione um arquivo explicando os passos para rodar as aplicações desenvolvidas. Se preferir e tiver acesso a algum tipo de hospedagem pode upar o projeto e nos passar o link + senha do acesso ao painel WordPress, mas deixe o código-fonte disponível no BitBucket de qualquer forma  


Entendemos que o teste pode ir de um nível simples até um nível um pouco mais complexo, não se preocupe em fazer absolutamente tudo. A intenção é ver até onde o candidato consegue chegar  


Boa sorte
